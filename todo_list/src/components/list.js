import { useState } from 'react';
import styled from 'styled-components/macro';
import ListItem from './listItem'
const List = () => {


       //state
        const[count,setCount] = useState(0)
        const[input,setInput] = useState('')
        const[list,setList] = useState([])
        const[button,setButton] = useState('All')
        const[amount,setAmount]=useState(0)
         
        const handleChange = (e) => {
         setInput(e.target.value)
        }
        
        const handleClick = (e) => {
           e.preventDefault()
           let item = {
             id: count,
             isComplited: false,
             isDelited:false,
             content: input
           }
           setCount(count+1)
           setList([...list,item])
           setAmount(amount+1)
           
       }

       const handleClickView = (e) => {
        if(e.target.name === 'All'){
            setButton('All')
        }
        else if(e.target.name === 'Active'){
            setButton('Active')
        }
        else if(e.target.name === 'Completed'){
            setButton('Completed')
        }
      }

      const dataFromChild = ({isComplited,isDelited,content,id}={}) => {
       let newList = list.map((item)=>{return item.id===id?{isComplited,isDelited,content,id}:item})
       setAmount(list.filter((item)=>!item.isComplited&&!item.isDelited).length)
       setList(newList)    
      }

    
       const cuurent_view = (item,myfilter)=>{
            if(myfilter){
          return <li> <ListItem  item = {item} callback = {dataFromChild} /> </li>
            }
       }

      const view = () =>{
         
        return list?.map(item => {
        switch (button){
            case 'Active':
                return cuurent_view(item,!item.isComplited&&!item.isDelited)
            case 'Completed':  
                return cuurent_view(item,item.isComplited&&!item.isDelited)
           default:
                return cuurent_view(item,!item.isDelited)
                }
            })
            }

        return (
            <>
           <FormDiv>
            <FormH1>Todo App</FormH1>
           <FormDiv2>
           <Form>
           <FormInput type="text" placeholder="Your next task" onChange={handleChange}/>
           <FormButton onClick={handleClick}>Add</FormButton>
           </Form>

                   <DivUl>
                    {view()}
                    </DivUl>
               { amount === 0  ? 
                 null : 
          
                    <Footer> 
                       <FooterH6>{amount} item left</FooterH6>
                       <DivButton>
                       <FooterButton name='All' onClick={handleClickView}>All</FooterButton>
                       <FooterButton name='Active' onClick={handleClickView}>Active</FooterButton>
                       <FooterButton name='Completed' onClick={handleClickView}>Completed</FooterButton>
                       </DivButton>
                    </Footer>
}
           </FormDiv2>
           </FormDiv>
            </>
        )
};

export default List;


const FormDiv = styled.div`
display:flex;
flex-flow:column wrap;
padding:10px;
`
const FormDiv2 = styled.div`
 display:flex;
 align-self: center;
 flex-flow:column wrap;
 box-shadow: 0rem 2rem 4rem rgb(0 0 0 / 30%);
 margin-top: 12px;
 background-color:white;
`
const FormH1 = styled.h1`
display:flex;
align-self: center;
margin-top: 10px;
`

const DivUl = styled.ul`
display:flex;
align-self: center;
flex-flow:column wrap;
min-width:688.5px;
max-width:688.5px;
padding:10px;
`
const Form = styled.form 
`display:flex;
 align-self: center;
 padding:10px;
 min-width:680px;
 margin-top: 12px;
 ` 

const FormInput= styled.input`
display:flex;
padding:1px;
padding: 0.5rem;
font-size: 25px;
align-self: center;
min-width:600px;
 ` 

 const FormButton = styled.button`
 padding: 1rem 2rem;
 `

 const Footer = styled.footer`
 display:flex;
 align-self: center;
 justify-content:space-between;
 min-width:680px;
 `
 const FooterH6 = styled.footer`
 display:flex;
 font-size:10px;
 `

 const DivButton = styled.footer`
 display:flex;
 font-size:10px;
 `
 const FooterButton = styled.button`
 margin-left: 0.2rem;
 font-size: 1rem;
 padding: 0.2rem 1rem;
 outline: none;
 border-radius: 0.3rem;
 border-width: 0.1rem;
 background: transparent;
 border-color: transparent;
 cursor: pointer;
 `