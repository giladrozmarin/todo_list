import React, { useState } from 'react';
import styled from 'styled-components';
import {
  RiCheckboxBlankCircleLine,
  RiCheckboxCircleLine,
} from 'react-icons/ri';
import { TiDeleteOutline } from 'react-icons/ti';

const ListItem = ({ item, callback, id}) => {
  let [isComplited, setIsComplited] = useState(false);
  let [isDelited, setIsDelited] = useState(item.isDelited);

  const handleCompliteButton = () => {
    
    setIsComplited(!isComplited);
    item.isComplited = !isComplited; 
     callback(item);
  };
  const handleDeleteButton = () => {

    setIsDelited(!isDelited);
    item.isDelited = !isDelited;
    callback(item);
  };

  let checkbox = isComplited ? (
    <Check onClick={handleCompliteButton} />
  ) : (
    <UnCheck onClick={handleCompliteButton} />
  );

  return (
    <ListItemBox>
      {checkbox}
      <ListItemContentBox complited={isComplited}>
        {item.content}
      </ListItemContentBox>
      <Remove onClick={handleDeleteButton} />
    </ListItemBox>
  );
};

export default ListItem;

const Check = styled(RiCheckboxCircleLine)`
  cursor: pointer;
  color: green;
`;

const UnCheck = styled(RiCheckboxBlankCircleLine)`
  cursor: pointer;
  color: gray;
`;

const CheckBox = styled.div`
  ${({ isComplited }) => (isComplited ? { Check } : { UnCheck })}
`;
export const Remove = styled(TiDeleteOutline)`
  cursor: pointer;
  color: red;
  visibility: hidden;
`;

const ListItemBox = styled.div`
  display: flex;
  align-items: center;
  border-bottom: solid lightgrey 1px;
  padding: 0.5rem 2rem;
  width: 100%;
  justify-content: center;
  line-height: 2.8rem;
  font-size: 25px;

  &:hover ${Remove} {
    visibility: visible;
  }
`;

const ListItemContentBox = styled.label`
  color: ${({ complited }) => (complited ? '#d9d9d9' : 'black')};
  text-decoration: ${({ complited }) => (complited ? 'line-through' : 'none')};
  word-wrap: break-word;
  width: 100%;
  margin: 0px 1rem;
`;
